name := "ddd-sarooja-v2"

version := "1.0"

scalaVersion := "2.11.7"


libraryDependencies ++= {
  val akkaV       = "2.4.4"
  val scalaTestV  = "2.2.6"

  Seq(
    "com.typesafe.akka" %% "akka-actor"          % akkaV,
    "com.typesafe.akka" %% "akka-testkit"        % akkaV % "test",
    "com.typesafe.akka" %% "akka-persistence"    % akkaV,
    "org.scalatest" %% "scalatest"               % scalaTestV % "test",
    "com.typesafe.akka" %% "akka-stream"         % akkaV,
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaV
  )
}
